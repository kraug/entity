package entity.test;

import entity.Command;
import entity.Entity;
import entity.memory.MemoryEntityStore;
import entity.memory.MemoryEventStore;
import entity.simple.SimpleCommandProcessor;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Stream.concat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class Fixture<I, E> {
    private final Function<I, Entity<I, E>> factory;
    private I id;
    private List<E> state = List.of();
    private List<Consumer<E>> listeners = List.of();
    private Command<I> command;

    private Fixture(Function<I, Entity<I, E>> factory) {
        this.factory = factory;
    }

    public static <I, E> Fixture<I, E> given(Function<I, Entity<I, E>> factory) {
        return new Fixture<>(factory);
    }

    @SafeVarargs
    public final Fixture<I, E> with(I id, E... events) {
        this.id = id;
        this.state = List.of(events);
        return this;
    }

    @SafeVarargs
    public final Fixture<I, E> with(Consumer<E>... listeners) {
        this.listeners = List.of(listeners);
        return this;
    }

    public Fixture<I, E> when(Command<I> command) {
        this.command = command;
        return this;
    }

    @SafeVarargs
    public final void then(final E... expected) {
        var entityStore = new MemoryEntityStore<>(factory);
        var eventStore = new MemoryEventStore<I, E>(Runnable::run);
        eventStore.store(id, state);
        var events = new LinkedList<E>();
        var futures = concat(Stream.of(events::add), listeners.stream())
                .map(eventStore::consume);
        new SimpleCommandProcessor<>(eventStore, entityStore).dispatch(command);
        futures.forEach(x -> x.complete(null));
        assertArrayEquals(Stream.concat(state.stream(), Stream.of(expected)).toArray(), events.toArray());
    }
}

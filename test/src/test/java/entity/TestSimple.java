package entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import static entity.test.Fixture.given;
import static java.util.UUID.randomUUID;

public class TestSimple {
    @Test
    public void shouldCreateNew() {
        given((UUID x) -> new Greeter(x, 0))
                .when(new GreetingCommand(randomUUID(), "george"))
                .then(new GreetingEvent("hello george"), new CountEvent(1));
    }

    @Test
    public void shouldUseExisting() {
        var id = randomUUID();
        given((UUID x) -> new Greeter(x, 0))
                .with(id, new GreetingEvent("hello sam"), new CountEvent(1))
                .when(new GreetingCommand(id, "george"))
                .then(new GreetingEvent("hello george"), new CountEvent(2));
    }

    @Test
    public void shouldTriggerListener() {
        var latch = new CountDownLatch(1);
        given((UUID x) -> new Greeter(x, 0))
                .with(x -> latch.countDown())
                .when(new GreetingCommand(randomUUID(), "george"))
                .then(new GreetingEvent("hello george"), new CountEvent(1));
        Assertions.assertEquals(0, latch.getCount());
    }

    record GreetingCommand(UUID id, String name) implements Command<UUID> {
    }

    interface GreeterEvent {
    }

    record GreetingEvent(String greeting) implements GreeterEvent {
    }

    record CountEvent(int count) implements GreeterEvent {
    }

    record Greeter(
            UUID id,
            int count
    ) implements Entity<UUID, GreeterEvent> {
        @Override
        public List<GreeterEvent> handle(Command<UUID> command) {
            if (command instanceof GreetingCommand c)
                return List.of(
                        new GreetingEvent("hello %s".formatted(c.name())),
                        new CountEvent(count + 1)
                );
            throw new IllegalArgumentException("Command not supported: %s".formatted(command.getClass().getName()));
        }

        @Override
        public Greeter apply(GreeterEvent event) {
            if (event instanceof CountEvent c)
                return new Greeter(id, c.count());
            return this;
        }
    }
}

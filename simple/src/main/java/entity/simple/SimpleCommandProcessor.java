package entity.simple;

import entity.*;

public class SimpleCommandProcessor<I, E> implements CommandProcessor<I> {
    private final EntityStore<I, E> entities;
    private final EventStore<I, E> events;

    public SimpleCommandProcessor(EventStore<I, E> events, EntityStore<I, E> entities) {
        this.events = events;
        this.entities = entities;
    }

    @Override
    public void dispatch(Command<I> command) {
        var id = command.id();
        var entity = entities.get(id);
        entity = events.load(x -> x.id().equals(id))
                .reduce(entity, Entity::apply, (x, y) -> y);
        var effects = entity.handle(command);
        var result = effects.stream()
                .reduce(entity, Entity::apply, (x, y) -> y);
        entities.put(id, result);
        events.store(id, effects);
    }
}

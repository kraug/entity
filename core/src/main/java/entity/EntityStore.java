package entity;

public interface EntityStore<I, E> {
    Entity<I, E> get(I id);

    void put(I id, Entity<I, E> entity);
}

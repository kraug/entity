package entity;

import java.util.List;

public interface Entity<I, E> {
    List<E> handle(Command<I> command);

    Entity<I, E> apply(E event);
}

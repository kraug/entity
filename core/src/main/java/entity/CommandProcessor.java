package entity;

public interface CommandProcessor<I> {
    void dispatch(Command<I> command);
}

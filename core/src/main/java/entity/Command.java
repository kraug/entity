package entity;

public interface Command<I> {
    I id();
}

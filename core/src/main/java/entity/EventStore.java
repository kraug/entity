package entity;

import java.util.List;
import java.util.concurrent.Flow;
import java.util.function.Predicate;
import java.util.stream.Stream;

public interface EventStore<I, E> extends Flow.Publisher<E> {
    void store(I id, List<E> events);

    Stream<E> load(Predicate<Event<I, E>> filter);

    record Event<I, E>(I id, long order, E payload) {
    }
}

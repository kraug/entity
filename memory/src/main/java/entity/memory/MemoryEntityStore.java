package entity.memory;

import entity.Entity;
import entity.EntityStore;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class MemoryEntityStore<I, E> implements EntityStore<I, E> {
    private final Map<I, Entity<I, E>> store = new ConcurrentHashMap<>();
    private final Function<I, Entity<I, E>> factory;

    public MemoryEntityStore(Function<I, Entity<I, E>> factory) {
        this.factory = factory;
    }

    @Override
    public Entity<I, E> get(I id) {
        return store.computeIfAbsent(id, factory);
    }

    @Override
    public void put(I id, Entity<I, E> entity) {
        store.put(id, entity);
    }
}

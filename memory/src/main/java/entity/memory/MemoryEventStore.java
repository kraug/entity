package entity.memory;

import entity.EventStore;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Flow;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toCollection;

public class MemoryEventStore<I, E> implements EventStore<I, E> {
    private final List<Event<I, E>> store = new LinkedList<>();
    private final List<Subscription> subscriptions = new LinkedList<>();
    private final Executor executor;

    public MemoryEventStore(Executor executor) {
        this.executor = executor;
    }

    @Override
    public void store(I id, List<E> events) {
        events.stream()
                .map(x -> new Event<>(id, store.size(), x))
                .collect(toCollection(() -> store));
        subscriptions.forEach(executor::execute);
    }

    @Override
    public Stream<E> load(Predicate<Event<I, E>> filter) {
        return store.stream()
                .filter(filter)
                .map(Event::payload);
    }

    @Override
    public void subscribe(Flow.Subscriber<? super E> subscriber) {
        var subscription = new Subscription(subscriber);
        subscriptions.add(subscription);
        subscriber.onSubscribe(subscription);
    }

    public CompletableFuture<Void> consume(Consumer<E> consumer) {
        var future = new CompletableFuture<Void>();
        subscribe(new ConsumerSubscriber<>(future, consumer));
        return future;
    }

    private class Subscription implements Flow.Subscription, Runnable {
        private final Flow.Subscriber<? super E> subscriber;
        private int index = 0;
        private long requested = 0;
        private boolean canceled = false;

        public Subscription(Flow.Subscriber<? super E> subscriber) {
            this.subscriber = subscriber;
        }

        @Override
        public void request(long n) {
            if (canceled) return;
            requested += n;
            executor.execute(this);
        }

        @Override
        public void cancel() {
            if (canceled) return;
            canceled = true;
            subscriptions.remove(this);
        }

        @Override
        public void run() {
            if (canceled) return;
            while (index < store.size() && requested-- > 0) {
                try {
                    subscriber.onNext(store.get(index++).payload());
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        }
    }

    static class ConsumerSubscriber<T> implements Flow.Subscriber<T> {
        private final CompletableFuture<Void> future;
        private final Consumer<? super T> consumer;
        private Flow.Subscription subscription;

        public ConsumerSubscriber(
                CompletableFuture<Void> future,
                Consumer<? super T> consumer
        ) {
            this.future = future;
            this.consumer = consumer;
        }

        public final void onSubscribe(Flow.Subscription subscription) {
            this.subscription = subscription;
            future.whenComplete((v, e) -> subscription.cancel());
            if (!future.isDone()) subscription.request(Long.MAX_VALUE);
        }

        public final void onError(Throwable throwable) {
            future.completeExceptionally(throwable);
        }

        public final void onComplete() {
            future.complete(null);
        }

        public final void onNext(T item) {
            try {
                consumer.accept(item);
            } catch (Throwable e) {
                subscription.cancel();
                future.completeExceptionally(e);
            }
        }
    }
}
